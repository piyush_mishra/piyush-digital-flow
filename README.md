#Graham Digital DVCS Flow

This is a repo dedicated to describing our team's process of creating feature and bugfix branches, merging them back
into dev and ultimately staging for release, then releasing to master.

## Set up your environment

```sh
git config --global branch.autosetuprebase always
git config --global pull.rebase true
```

These commands set up rebasing automatically on your computer and also automatically rebases when pulling and your tree
is out of sync. These changes will ensure you don't create merge commits when just pushing and pulling.

## Creating a feature or bugfix branch

All work is done in branches off of the `dev` branch. You can create a new feature branch from the command line:
```sh
# First checkout the dev branch
git checkout dev

# Create a new branch to work from, include the ticket number if there is one and the feature name
git checkout -b mn/DI-10_new-feature
```

For organization, putting your initials as the folder will make it easier to identify work in the future.

Jira also has the capability to create the branches for you. You have the opportunity to add the initials ahead of the
branch name. Follow the instructions to check the branch out from remote.

![Creating Branches in Jira](https://bytebucket.org/grahamdigital/graham-digital-flow/raw/41e3f3ed63273813ed7b73d9dbb2e69acd3e679e/images/jira-branch-create.png)

## Creating code and committing

All commits should be done in these working branches.

All commits should be:
* Atomic, meaning all code should be able to be added and removed only adding the feature or removing the bug without
any other changes.
* Concise, commits should be the smallest granular change possible.

Commit messages should contain:
* What is being changed
* Why it is being changed
* The ticket number in (Paratheses) after the message

Commit messages should not contain:
* Anything that is time sensitive: "Fixing the issue Michael raised"
* Inside baseball: "Fixing the bug Michael described"
* Duplicate of the ticket name: "Describe the Graham Digital Git Branch and Commit Flow (DI-10)"
* The files being changed
* The actual code


### Keep your branch up to date!!!

As a branch is being worked on, the developer should continue to get updates from the `dev` branch as other developers
make changes. It is the developer's job to make sure her branch applies cleanly. To do this cleanly without introducing
additional commits, rebasing from `dev` to the feature branch:

```sh
# Make sure you are on the feature branch
git co mn/DI-10_new-feature

git rebase dev
# Current branch mn/DI-10_new-feature is up to date.
```

In the event of a conflict, you will get a message like:

```sh
$ git rebase dev
First, rewinding head to replay your work on top of it...
Applying: Adding a cool new feature
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging file
CONFLICT (add/add): Merge conflict in file
Failed to merge in the changes.
Patch failed at 0001 Adding a cool new feature
The copy of the patch that failed is found in:
   /graham-digital-flow/.git/rebase-apply/patch

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".

$ git st
UU file

```

If this happens, you have to merge manually. Open the conflicted file(s) (in this case the file's name is `file`). Make the
changes. You then have the add the file to mark it resolved and then continue the rebase:

```sh
git add file

git rebase --continue
```

The longer you have between merges the more times this type of conflict can occur. Keep your branch up to date with dev.


## Merging the code to dev

Once the code has been reviewed in the ticket and branch, it is time to move the code to master from the branch. Tests,
documentation and final code should be in the branch. Rebase from `dev`. The comment should be `Current branch
mn/DI-10_new-feature is up to date.`

Important: do not go forward until your rebase from `dev` provides this message.

Check out `dev` and "merge" the feature branch:

```sh
git co dev

git merge mn/DI-10_new-feature
# Updating 8475fd6..f4dfb98
# Fast-forward
#  file1 | 2 +-
#  1 file changed, 1 insertion(+), 1 deletion(-)
```

> This is technically not a merge, but a fast-forward, even though the command is merge no new commits are added,
> because the branch is a direct descendant of `dev` due to the rebasing in the branch. Because of this, no actual
> merges are needed, all the work is done in the existing commits.

We do not want to have merge commits. These can lead to sloppiness and are difficult to work out of.

## Clean up after yourself

After this is done all the code is in the `dev` branch, don't forget to push your changes.

Your branches don't automatically clean themselves. You have to delete these manually. To locally delete branches:

```sh
git co dev

git branch -d mn/DI-10_new-feature
```

This will not delete a branch where all commits are not represented in another branch.

If you pushed your branch to the remote repository or used Jira to create the branch, you have to delete your branches
on remote. To do that:

```sh
git push origin :mn/DI-10_new-feature
```

This will keep the repository clean.

## Putting this all together

You should test these processes so you are comfortable on this repository.
[Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository) this repository and clone it locally.
From the codes directory:

```sh
$ git clone git@bitbucket.org:newmaniese/graham-digital-flow.git
Cloning into 'graham-digital-flow'...
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 7 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (7/7), 72.00 KiB | 0 bytes/s, done.
Checking connectivity... done.

~/Projects
└$ cd graham-digital-flow

~/Projects/graham-digital-flow (master)
└$ git co dev
Switched to branch 'dev'
Your branch is up-to-date with 'origin/dev'.

~/Projects/graham-digital-flow (dev)
└$ git co -b mn/new-feature
Switched to a new branch 'mn/new-feature'

~/Projects/graham-digital-flow (mn/new-feature)
└$ touch file1

~/Projects/graham-digital-flow (mn/new-feature?)
└$ git add file1

~/Projects/graham-digital-flow (mn/new-feature●)
└$ git commit -m"Adding a new file to show how"
[mn/new-feature 90630f8] Adding a new file to show how
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file1

~/Projects/graham-digital-flow (mn/new-feature)
└$ git rebase dev
Current branch mn/new-feature is up to date.

~/Projects/graham-digital-flow (mn/new-feature)
└$ git co dev
Switched to branch 'dev'
Your branch is up-to-date with 'origin/dev'.

~/Projects/graham-digital-flow (dev)
└$ git merge mn/new-feature
Updating 9d7ba58..90630f8
Fast-forward
 file1 | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file1

# Good now let's work on a more complicated feature
~/Projects/graham-digital-flow (dev)
└$ git co -b mn/newer-feature
Switched to a new branch 'mn/newer-feature'

~/Projects/graham-digital-flow (mn/newer-feature)
└$ echo "This is some code" > file2

~/Projects/graham-digital-flow (mn/newer-feature?)
└$ git add file2

~/Projects/graham-digital-flow (mn/newer-feature●)
└$ git commit -m"Adding some code in file2, this needs to go through review, might take a while before available on dev"
[mn/newer-feature 804c47a] Adding some code in file2, this needs to go through review, might take a while before available on dev
 1 file changed, 1 insertion(+)
 create mode 100644 file2

# I am getting tired, pushing before calling it a night
~/Projects/graham-digital-flow (mn/newer-feature)
└$ git push
fatal: The current branch mn/newer-feature has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin mn/newer-feature

# Oops I have to create the branch upstream
~/Projects/graham-digital-flow (mn/newer-feature)
└$ git push --set-upstream origin mn/newer-feature
Counting objects: 6, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (6/6), 571 bytes | 0 bytes/s, done.
Total 6 (delta 1), reused 0 (delta 0)
To git@bitbucket.org:newmaniese/graham-digital-flow.git
 * [new branch]      mn/newer-feature -> mn/newer-feature
Branch mn/newer-feature set up to track remote branch mn/newer-feature from origin by rebasing.

# Overnight someone else is making changes to the file I am working on in my branch
~/Projects/graham-digital-flow (master)
└$ git co dev
Switched to branch 'dev'

~/Projects/graham-digital-flow (dev)
└$ git co -b mn/newest-feature
Switched to a new branch 'mn/newest-feature'

~/Projects/graham-digital-flow (mn/newest-feature)
└$ echo "This is some diff. code" > file2

~/Projects/graham-digital-flow (mn/newest-feature?)
└$ git add file2

~/Projects/graham-digital-flow (mn/newest-feature●)
└$ git commit -m"Adding some code to file2, I am quicker and will merge sooner"
[mn/newest-feature 7160ebb] Adding some code to file2, I am quicker and will merge sooner
 1 file changed, 1 insertion(+)
 create mode 100644 file2

~/Projects/graham-digital-flow (mn/newest-feature)
└$ git rebase dev
Current branch mn/newest-feature is up to date.

~/Projects/graham-digital-flow (mn/newest-feature)
└$ git co dev
Switched to branch 'dev'

# Thier code gets to dev faster than the original code
~/Projects/graham-digital-flow (dev)
└$ git rebase mn/newest-feature
First, rewinding head to replay your work on top of it...
Fast-forwarded dev to mn/newest-feature.


# Next morning, ready to work on the newer-feature again
~/Projects/graham-digital-flow (dev)
└$ git co mn/newer-feature
Switched to branch 'mn/newer-feature'
Your branch is up-to-date with 'origin/mn/newer-feature'.

# O no, there is a conflict during the rebase
~/Projects/graham-digital-flow (mn/newer-feature)
└$ git rebase dev
First, rewinding head to replay your work on top of it...
Applying: Adding some code in file2, this needs to go through review, might take a while before available on dev
Using index info to reconstruct a base tree...
Falling back to patching base and 3-way merge...
Auto-merging file2
CONFLICT (add/add): Merge conflict in file2
Failed to merge in the changes.
Patch failed at 0001 Adding some code in file2, this needs to go through review, might take a while before available on dev
The copy of the patch that failed is found in:
   /Users/newmaniese/Projects/graham-digital-flow/.git/rebase-apply/patch

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".

~/Projects/graham-digital-flow (rebase)(mn/newer-feature!●)
└$ git st
AA file2

# file2 is in conflict during the rebase
~/Projects/graham-digital-flow (rebase)(mn/newer-feature!●)
└$ cat file2
<<<<<<< HEAD
This is some diff. code
=======
This is some code
>>>>>>> Adding some code in file2, this needs to go through review, might take a while before available on dev

# Let me clean the conflict
~/Projects/graham-digital-flow (rebase)(mn/newer-feature!●)
└$ echo "This is some different code" > file2

#And mark the file as resolved
~/Projects/graham-digital-flow (rebase)(mn/newer-feature!●)
└$ git add file2

~/Projects/graham-digital-flow (rebase)(mn/newer-feature●)
└$ git st
M  file2

~/Projects/graham-digital-flow (rebase)(mn/newer-feature●)
└$ git rebase --continue
Applying: Adding some code in file2, this needs to go through review, might take a while before available on dev

~/Projects/graham-digital-flow (mn/newer-feature)
└$ git rebase dev
Current branch mn/newer-feature is up to date.

# Good, all up to date
newmaniese@Newmanieses-MacBook-Pro:~/Projects/graham-digital-flow (mn/newer-feature)
└$ git co dev
Switched to branch 'dev'

~/Projects/graham-digital-flow (dev)
└$ git merge mn/newer-feature
Updating 7160ebb..3163978
Fast-forward
 file2 | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

~/Projects/graham-digital-flow (dev)
└$ git push
# All set!


# time to clean up
$ git branch -l
* dev
  master
  mn/new-feature
  mn/newer-feature
  mn/newest-feature

~/Projects/graham-digital-flow (dev)
└$ git branch -d mn/new-feature
Deleted branch mn/new-feature (was 90630f8).

~/Projects/graham-digital-flow (dev)
└$ git branch -d mn/newest-feature
Deleted branch mn/newest-feature (was 7160ebb).

~/Projects/graham-digital-flow (dev)
└$ git branch -d mn/newer-feature
warning: not deleting branch 'mn/newer-feature' that is not yet merged to
         'refs/remotes/origin/mn/newer-feature', even though it is merged to HEAD.
error: The branch 'mn/newer-feature' is not fully merged.
If you are sure you want to delete it, run 'git branch -D mn/newer-feature'.

# wait something is wrong: I didn't push this branch to remote after I rebased
~/Projects/graham-digital-flow (dev)
└$ git co mn/newer-feature
Switched to branch 'mn/newer-feature'
Your branch and 'origin/mn/newer-feature' have diverged,
and have 2 and 1 different commit each, respectively.
  (use "git pull" to merge the remote branch into yours)

~/Projects/graham-digital-flow (mn/newer-feature)
└$ git push
To git@bitbucket.org:newmaniese/graham-digital-flow.git
 ! [rejected]        mn/newer-feature -> mn/newer-feature (non-fast-forward)
error: failed to push some refs to 'git@bitbucket.org:newmaniese/graham-digital-flow.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.

# When I rebased, I changed a commit, so git doesn't know what to do
# I can force git to accept my changes.
# This is destructive, only do it on your branches that you know no one
# else has touched, you will lose any changes not on your local machine
~/Projects/graham-digital-flow (mn/newer-feature)
└$ git push --force
Total 0 (delta 0), reused 0 (delta 0)
To git@bitbucket.org:newmaniese/graham-digital-flow.git
 + 804c47a...3163978 mn/newer-feature -> mn/newer-feature (forced update)

~/Projects/graham-digital-flow (mn/newer-feature)
└$ git co dev
Switched to branch 'dev'
Your branch is up-to-date with 'origin/dev'.

~/Projects/graham-digital-flow (dev)
└$ git branch -d mn/newer-feature
Deleted branch mn/newer-feature.

# Finally delete the remote branch
~/Projects/graham-digital-flow (mn/newer-feature)
└$ git push origin :mn/newer-feature
To git@bitbucket.org:newmaniese/graham-digital-flow.git
 - [deleted]         mn/newer-feature
```
